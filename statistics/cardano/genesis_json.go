package cardano

import (
	"log"
	"time"

	"gitlab.com/encryptoteam/rocket-apps/services/informer/helpers"
)

const (
	urlGenesisBase = "https://book.world.dev.cardano.org/environments/mainnet/"
	shelleyName    = "shelley-genesis.json"
	byronName      = "byron-genesis.json"
	alonzoName     = "alonzo-genesis.json"
)

type genesisJSONFiles struct {
	shelleyGenesis []byte
	alonzoGenesis  []byte
	byronGenesis   []byte
}

// startUpdateTimeoutCycle - open new goroutine and update struct fields
func (f *genesisJSONFiles) startUpdateTimeoutCycle(timeoutHours int) {
	fDownload := func() {
		file, err := helpers.DownloadFile(shelleyName, urlGenesisBase)
		if err != nil {
			log.Println(err)
		} else {
			f.shelleyGenesis = file
		}

		file, err = helpers.DownloadFile(byronName, urlGenesisBase)
		if err != nil {
			log.Println(err)
		} else {
			f.byronGenesis = file
		}

		file, err = helpers.DownloadFile(alonzoName, urlGenesisBase)
		if err != nil {
			log.Println(err)
		} else {
			f.alonzoGenesis = file
		}
	}

	fDownload()
	go func() {
		// c := time.Tick(time.Duration(timeoutHours))
		// for range c {
		// 	fDownload()
		// }

		for {
			time.Sleep(time.Duration(timeoutHours) * time.Hour)
			fDownload()
		}
	}()
}
