package cardano

import (
	"github.com/tidwall/gjson"
	pb "gitlab.com/encryptoteam/rocket-apps/services/proto/proto-gen/cardano"
)

// GetEpoch -
func (cardano *Cardano) GetEpoch(jsonBody string) *pb.Epoch {
	var epoch pb.Epoch
	epoch.EpochNumber = gjson.Get(jsonBody, "cardano.node.metrics.epoch.int.val").Int()
	return &epoch
}
