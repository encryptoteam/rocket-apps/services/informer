package tls

import (
	"crypto/tls"
	"crypto/x509"
	"fmt"
	"log"
	"os"

	"google.golang.org/grpc/credentials"
)

const (
	// CA certificate file name
	caCertFile = "ca-cert.pem"

	// Client certificate file name
	clientCertFile = "client-cert.pem"

	// Client private key file name
	clientKeyFile = "client-key.pem"
)

func LoadTLSCredentials(path string, tlsType string) (credentials.TransportCredentials, error) {
	config := &tls.Config{}

	switch tlsType {
	case "mutual":
		log.Println("using mutual TLS")

		// Load certificate of the CA who signed server's certificate
		pemServerCA, err := os.ReadFile(path + "/" + caCertFile)
		if err != nil {
			return nil, err
		}

		certPool := x509.NewCertPool()
		if !certPool.AppendCertsFromPEM(pemServerCA) {
			return nil, fmt.Errorf("failed to add server CA's certificate")
		}

		// Create the credentials and return it
		config.RootCAs = certPool

		// Load client's certificate and private key
		clientCert, err := tls.LoadX509KeyPair(path+"/"+clientCertFile, path+"/"+clientKeyFile)
		if err != nil {
			return nil, err
		}

		config.Certificates = []tls.Certificate{clientCert}

	case "server":
		log.Println("using server-side TLS")

		// Load certificate of the CA who signed server's certificate
		pemServerCA, err := os.ReadFile(path + "/" + caCertFile)
		if err != nil {
			return nil, err
		}

		certPool := x509.NewCertPool()
		if !certPool.AppendCertsFromPEM(pemServerCA) {
			return nil, fmt.Errorf("failed to add server CA's certificate")
		}

		// Create the credentials and return it
		config.RootCAs = certPool

	default:
		log.Println("using insecure connection")
		config.InsecureSkipVerify = true
	}

	return credentials.NewTLS(config), nil
}
