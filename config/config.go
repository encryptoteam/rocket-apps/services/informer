package config

import (
	"log"

	"github.com/bykovme/goconfig"
)

const cConfigPath = "informer.conf"
const cConfigPathReserve = "/etc/ada-rocket/informer.conf"

// Config - structure of config file
type Config struct {
	Ticker string `json:"ticker"`
	Type   string `json:"type"`
	Name   string `json:"name"`

	PathToCerts string `json:"path_to_certs"` //
	TLSType     string `json:"tls_type"`      // mutual, server, none

	UUID                    string `json:"uuid"`
	Location                string `json:"location"`
	ControllerURL           string `json:"controller_url"`
	TimeForFrequentlyUpdate int    `json:"time_for_frequently_update"`
	TimeForRareUpdate       int    `json:"time_for_rare_update"`

	NodeMonitoringURL string `json:"node_monitoring_url"`

	Blockchain string `json:"blockchain"`
}

func LoadConfig() (loadedConfig *Config, err error) {
	log.Println("Start loading config...")
	usrHomePath, err := goconfig.GetUserHomePath()
	if err != nil {
		return loadedConfig, err
	}

	loadedConfig = new(Config)

	err = goconfig.LoadConfig(cConfigPath, &loadedConfig)
	if err == nil {
		return loadedConfig, nil
	}

	log.Println("Config", cConfigPath, "not found")
	log.Println("Trying", usrHomePath+cConfigPathReserve, "reserve config path...")

	err = goconfig.LoadConfig(usrHomePath+cConfigPathReserve, loadedConfig)
	if err != nil {
		log.Println(err)
		loadedConfig, err = startCli(usrHomePath)
	}

	return loadedConfig, err
}
