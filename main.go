package main

import (
	"log"
	"time"

	"google.golang.org/grpc"
	"google.golang.org/grpc/credentials/insecure"
	"google.golang.org/grpc/grpclog"

	"gitlab.com/encryptoteam/rocket-apps/services/informer/config"
	"gitlab.com/encryptoteam/rocket-apps/services/informer/statistics/cardano"
	"gitlab.com/encryptoteam/rocket-apps/services/informer/tls"
)

func main() {
	// grpc.EnableTracing = true

	startTime := time.Now()
	loadedConfig, err := config.LoadConfig()
	if err != nil {
		log.Panicln(err)
		return
	}

	tlsCredentials, err := tls.LoadTLSCredentials(loadedConfig.PathToCerts, loadedConfig.TLSType)
	if err != nil {
		log.Println("cannot load TLS credentials: ", err)
		log.Println("using insecure connection")
		tlsCredentials = insecure.NewCredentials()
	}

	conn, err := grpc.Dial(
		loadedConfig.ControllerURL,
		grpc.WithTransportCredentials(tlsCredentials),
	)
	if err != nil {
		grpclog.Fatalf("fail to dial: %v", err)
	}
	defer conn.Close()

	switch loadedConfig.Blockchain {
	case "cardano":
		cardano.NewCardano(loadedConfig, startTime, conn)
	default:
		log.Println("Blockchain \"" + loadedConfig.Blockchain + "\" not supported")
		return
	}
}
